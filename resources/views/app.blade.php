<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body>
        <div id="sample_id"></div>
        <br />
        <div id="current_date"><div>
        {{-- <div id="sample_id2"></div> --}}
        <script src="{{ asset('js/app.js')}}"></script>
        {{-- <script src="{{ asset('js/components/Login.js')}}"></script> --}}
        {{-- <script src="{{ asset('js/components/Header.js')}}"></script> --}}
        {{-- <script src="{{ resource_path('js/components/Header.js')}}"></script> --}}
    </body>
</html>
