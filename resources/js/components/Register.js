import React,{useState} from 'react';

function Register() {
    const [name,setName] = useState("")
    const [password,setPassword] = useState("")
    const [email,setEmail] = useState("")
    const [count, setCount] = useState(11);

    async function signUp()
    {
        var item = {name,password,email};
        console.log(item);
        let result = await fetch("http://testapp.test/api/register",{
            method : 'POST',
            body : JSON.stringify(item),
            headers : {
                // "[{"key" : "Content-Type","value" : "application/json", "description" : ""}]"
                "Content-Type" : "application/json",
                "Accept" : 'application/json'
            }
        });

        result = await result.json();
        console.log("result",result);
    }

    return (
        <div className="col-sm-6 offset-sm-3">
            <h1>Register Page</h1>
            <input type="text" value={name} onChange={(e)=>setName(e.target.value)} className="form-control" placeholder="name"/>
            <br />
            <input type="password" value={password} onChange={(e)=>setPassword(e.target.value)} className="form-control" placeholder="password"/>
            <br />
            <input type="text" value={email} onChange={(e)=>setEmail(e.target.value)} className="form-control" placeholder="email"/>
            <br />
            <button onClick={signUp} className="btn btn-primary">Sign Up</button>

            <p>You clicked {count} times</p>
            <button onClick={() => setCount(count + 1)}>
                Click me
            </button>
        </div>
    );
}

export default Register;