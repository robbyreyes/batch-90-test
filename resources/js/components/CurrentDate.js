import React, { useState,useEffect } from 'react';

function CurrentDate() {

    
    const time_const = () => {
        let time = new Date().toLocaleTimeString();

        return time;
    };

    const [current_time,setCurrentTime] = useState(time_const());

    useEffect(()=> {
        const timer = setTimeout(() => {
            // console.log(time_const());
            setCurrentTime(time_const());
        }, 1000);
    })

    return (
        <div>
            {/* <h2 id="time">{new Date().toLocaleTimeString()}</h2> */}
            <h2 id="time">{current_time}</h2>
        </div>
    );
}

export default CurrentDate;