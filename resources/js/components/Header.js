import React from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'react-bootstrap';
import {Navbar,Nav,Form,FormControl} from 'react-bootstrap';
import CurrentDate from './CurrentDate';

function Header() {
    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand href="#home">Test App</Navbar.Brand>
                <Nav className="mr-auto navbar_wrapper">
                <Link to="/add">Add Product</Link>
                <Link to="/update">Update Product</Link>
                <Link to="/login">Login</Link>
                <Link to="/register">Register</Link>

                </Nav>
                <CurrentDate />
                <Form inline>
                <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                <Button variant="outline-light">Search</Button>
                </Form>
            </Navbar>
        </div>
    );
}

// setInterval(CurrentDate, 1000);

export default Header;

if (document.getElementById('current_date')) {
    ReactDOM.render(<CurrentDate />, document.getElementById('current_date'));
}