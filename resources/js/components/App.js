import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'react-bootstrap';
import {Navbar,Nav,Form,FormControl} from 'react-bootstrap';
import Header from './Header';
import './App.css';
import {BrowserRouter,Route} from 'react-router-dom'
import Login from './Login';
import Register from './Register';
import AddProduct from './AddProduct';
import UpdateProduct from './UpdateProduct';

function App() {
    return (
        <div>
            <BrowserRouter>
                <Header />
                <h1>Test Project</h1>
                <Route path="/login">
                    <Login />
                </Route>
                <Route path="/register">
                    <Register />
                </Route>
                <Route path="/add">
                    <AddProduct />
                </Route>
                <Route path="/update">
                    <UpdateProduct />
                </Route>
            </BrowserRouter>
        </div>
    );
}

export default App;

if (document.getElementById('sample_id')) {
    ReactDOM.render(<App />, document.getElementById('sample_id'));
}